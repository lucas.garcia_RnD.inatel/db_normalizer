

def get_db_template(template):

    print(f'Template selecionado: {template}')
    try:
        # ============== Facebook Template
        if str(template).upper() == 'FACEBOOK':
            #escolher em qual schema vai trabalhar
            schema = 'facebook'
            #configurar nome das tabelas como chave do dict e colunas como lista de valores para cada chave
            table_colum_dict = {
                'Regional_Maps_Download_Speed':
                    ['CIDADE'],
                'Regional_Maps_Downstream_Latency':
                    ['CIDADE'],
                'Regional_Maps_Round_Trip_Time':
                    ['CIDADE']
            }
            return schema, table_colum_dict
        # ============== 4g ip reassembly
        if str(template).upper() == 'REASSEMBLY':
            #escolher em qual schema vai trabalhar
            schema = 'npm'
            #configurar nome das tabelas como chave do dict e colunas como lista de valores para cada chave
            table_colum_dict = {
                'tx_4g_accedian_day_city':
                    ['CITY'],
                'vw_reassembly_day_city_lte':
                    ['CITY'],
            }
            return schema, table_colum_dict
    except Exception as e:
        print(f'Erro ao carregar template! Verifique o template selecionado.')
        exit()