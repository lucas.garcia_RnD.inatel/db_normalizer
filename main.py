import os
import mysql.connector as mysql
from datetime import datetime

from templates import get_db_template


def get_sql_template():
    try:
        basepath = os.path.dirname(__file__)
        script_path = os.path.abspath(os.path.join(basepath, 'NORMALIZADOR_V1.sql'))
        #faz leitura do template
        # Open and read the file as a single buffer
        fd = open(script_path, 'r', encoding='utf-8')
        script = fd.read()
        fd.close()
        print(f'Template SQL carregado com sucesso!')
        return script
    except Exception as e:
        print(f'Erro ao carregar template SQL! Error:{e}')
        exit()


def conecta_mariadb():
    try:
        con_mariadb = mysql.connect(
            host="10.26.82.160",
            user='lrgsouza',
            passwd='luc4S2020',
            allow_local_infile=True
        )
        return con_mariadb
    except Exception as e:
        print(f'Erro na conexão com o banco de dados! Verifique sua VPN! Error:{e}')
        exit()

def normalize(script, schema, table_colum_dict):
    #conectando ao db
    con_bacos = conecta_mariadb()
    cursor = con_bacos.cursor()

    #manipulando script para normalizar
    #salvando copia do template em variavel já com schema
    #setando schema que vai trabalhar
    saved_tamplate = script.replace('schema',schema)
    for table in table_colum_dict:
        print(f'WORKING ON {schema}.{table}')
        #configurando tabela
        new_template = saved_tamplate.replace('table',table)
        #loop para configurar colunas
        for column in table_colum_dict[table]:
            # Execute every command from the input file
            query_template = new_template.replace('column',column)
            sqlCommands = query_template.split(';')
            for command in sqlCommands:
                # This will skip and report errors
                # For example, if the tables do not yet exist, this will skip over
                # the DROP TABLE commands
                try:
                    cursor.execute(command.strip())
                    con_bacos.commit()
                except Exception as e:
                    print(f'Command skipped: {e}')
    con_bacos.close()



# Press the green button in the gutter to run the script.
if __name__ == '__main__':

    print(f'>>>>>> START PROCESS <<<<<<<')
    start = datetime.now()

    #lendo template sql
    script = get_sql_template()

    #GET SCHEMA/TABLE/COLUMNS TEMPLATE [facebook, reassembly]
    schema, table_colum_dict = get_db_template('facebook')

    #chamando função de normalização
    normalize(script,schema,table_colum_dict)

    #finish
    final = datetime.now()
    delta = final - start
    delta = str(delta).split('.')[0]
    print(f'>>>>>> FINISH PROCESS in {delta}<<<<<<<')


